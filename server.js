const mysql = require("mysql");
const express = require("express");
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'feedmeback'
});


connection.connect(error => {
    if (error) throw error;
    console.log("Successfully connected to the database.");
});

app.get('/events', function (req, resp) {
    connection.query("select * from events", function (err, result, fields) {
        if (err) throw err;
        resp.send(result);
    });
});

app.post('/event', function (req, resp) {

    let requestBody = req.body;
    let title = requestBody.title;
    let description = requestBody.description;
    let startDate = requestBody.startDate;
    let endDate = requestBody.endDate;
    let schedule = requestBody.schedule;
    let creator = requestBody.creator;

    let sql = "INSERT INTO events (title, description, startDate, endDate, schedule, creator) VALUES ('" + title + "','" + description + "','" + startDate + "','" + endDate + "','" + schedule + "','" + creator + "')";
    connection.query(sql, function (err, result, fields) {
        if (err) throw err;
        resp.send('New event added');
        console.log('New event added: '+ title);
    });
});


app.listen(3000, () => {
    console.log("Server is running on port 3000.");
});