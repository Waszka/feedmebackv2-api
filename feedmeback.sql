-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 28 Sty 2020, 21:40
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `feedmeback`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `startDate` timestamp NULL DEFAULT NULL,
  `endDate` timestamp NULL DEFAULT NULL,
  `schedule` text COLLATE utf8_polish_ci NOT NULL,
  `creator` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `events`
--

INSERT INTO `events` (`id`, `title`, `description`, `startDate`, `endDate`, `schedule`, `creator`) VALUES
(1, 'barcoders', 'tak', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'dsadasdasdasd', 'anrdzej\r\n'),
(2, 'barcoders 2', 'dupa', '2020-01-27 23:00:00', '2020-01-29 23:00:00', 'plan', 'stefan'),
(3, 'Deweloperska baza', 'Deweloperska baza', '2020-01-27 23:00:00', '2020-01-27 23:00:00', 'Deweloperska baza', 'Deweloperska baza'),
(4, 'Deweloperska baza11', 'Deweloperska baza', '2020-01-27 23:00:00', '2020-01-27 23:00:00', 'Deweloperska baza', 'Deweloperska baza'),
(5, 'Deweloperska baza112', 'Deweloperska baza', '2020-01-27 23:00:00', '2020-01-27 23:00:00', 'Deweloperska baza', 'Deweloperska baza'),
(6, 'Testowiro', 'Deweloperska baza', '2020-01-27 23:00:00', '2020-01-27 23:00:00', 'Deweloperska baza', 'Deweloperska baza');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
